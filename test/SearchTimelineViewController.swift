//
//  SearchTimelineViewController.swift
//  test
//
//  Created by Rosen Mihaylov on 21.03.18.
//  Copyright © 2018 Rosen Mihaylov. All rights reserved.
//

import Foundation

import UIKit
import TwitterKit


class SearchTimelineViewController:  UIViewController {

    var client:TWTRAPIClient?
    weak var vc:TWTRTimelineViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.client = TWTRAPIClient()
        self.view.backgroundColor = UIColor.white
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        vc = segue.destination as? TWTRTimelineViewController
    }
}

extension SearchTimelineViewController:UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let str = searchBar.text,  let c = self.client, let v = self.vc {
            v.dataSource = TWTRSearchTimelineDataSource(searchQuery: str, apiClient: c)
        }
    }
}
